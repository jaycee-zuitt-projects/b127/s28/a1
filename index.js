// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
//Answer:
fetch('https://jsonplaceholder.typicode.com/todos')
	.then(res => res.json())
 	.then(json => console.log(json))

//4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
//Answer:
fetch('https://jsonplaceholder.typicode.com/todos')
.then(res => res.json())
.then(data => {
	let title = data.map(todos => {
		return todos.title
	})
	console.log(title)
})

//5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
//Answer:
fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then(res => res.json())
 	.then(json => console.log(json))

//6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
//answer
fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then(res => res.json())
 	.then(json => console.log(`The title is ${json.title} and the status is ${json.completed}`))

//7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
//answer
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'Poor Dad Rich Dad',
		completed: false
	})
})

.then(res => res.json())
.then(data => console.log(data))

//8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
//Answer:
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'Poor Dad Rich Dad',
		completed: false
	})
})

.then(res => res.json())
.then(data => console.log(data))

/*9. Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID*/
//Answer:
fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PUT',
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify({
		userId: 2,
		title: 'Rich Dad Poor Dad',
		description: 'What the rich teach their kids about money and that poor and middle class do not',
		completed: true,
		dateCompleted: "04/01/2017"
	})
})

.then(res => res.json())
.then(data => console.log(data))

//10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
//Answers:
fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'PATCH',
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Exercise and Read Books',
	})
})

.then(res => res.json())
.then(data => console.log(data))

//11. Update a to do list item by changing the status to complete and add a date when the status was changed.
fetch('https://jsonplaceholder.typicode.com/todos/4', {
	method: 'PATCH',
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		date: "2021-09-14"
	})
})

.then(res => res.json())
.then(data => console.log(data))

//12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
//Answer
fetch('https://jsonplaceholder.typicode.com/todos/5', {
	method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))

/*POSTMAN:
13. Create a request via Postman to retrieve all the to do list items.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as get all to do list items
14. Create a request via Postman to retrieve an individual to do list item.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as get to do list item
15. Create a request via Postman to create a to do list item.
- POST HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as create to do list item
16. Create a request via Postman to update a to do list item.
- PUT HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as update to do list item PUT
- Update the to do list item to mirror the data structure used in the PUT fetch request
17. Create a request via Postman to update a to do list item.
- PATCH HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as create to do list item
- Update the to do list item to mirror the data structure of the PATCH fetch request
18. Create a request via Postman to delete a to do list item.
- DELETE HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as delete to do list item
19. Export the Postman collection and save it in the activity folder.*/